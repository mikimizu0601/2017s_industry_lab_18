package ictgradschool.industry.lab18.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * TODO Have fun :)
 */
public class Program extends JFrame {

    private static final Random r = new Random();
    private Canvas c = new Canvas();
    private int currentDirection;
    private int fx = -1, fy = -1;
    private ArrayList<Integer> redBoxX = new ArrayList<>();
    private ArrayList<Integer> snakeX = new ArrayList<>();
    private ArrayList<Integer> redBoxY = new ArrayList<>();
    private ArrayList<Integer> snakeY = new ArrayList<>();
    private boolean done = false;


    public static void main(String[] args) {
        new Program().go();
    }

    public Program() {

        for (int i = 0; i < 6; i++) {
            snakeX.add(10 - i);
            snakeY.add(10);
        }
        this.currentDirection = 39;
        setTitle("Program" + " : " + 6);//r.snakeX
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
        setResizable(false);
        c.setBackground(Color.white);
        add(BorderLayout.CENTER, c);

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int nextDirection = e.getKeyCode();
                if ((nextDirection >= 37) && (nextDirection <= 40)) {// block wrong codes
                    if (Math.abs(Program.this.currentDirection - nextDirection) != 2) {// block moving back
                        Program.this.currentDirection = nextDirection;
                    }
                }
            }
        });
        setVisible(true);
    }

    void go() { // main loop
        while (!done) {
            int snakeHeadX = snakeX.get(0);
            int snakeHeadY = snakeY.get(0);
            if (currentDirection == 37) {
                snakeHeadX--;
            }
            if (currentDirection == 39) {
                snakeHeadX++;
            }
            if (currentDirection == 38) {
                snakeHeadY--;
            }
            if (currentDirection == 40) {
                snakeHeadY++;
            }
            if (snakeHeadX > 30 - 1) {
                snakeHeadX = 0;
            }
            if (snakeHeadX < 0) {
                snakeHeadX = 30 - 1;
            }
            if (snakeHeadY > 20 - 1) {
                snakeHeadY = 0;
            }
            if (snakeHeadY < 0) {
                snakeHeadY = 20 - 1;
            }
            boolean check11 = false;
            boolean check21 = false;
            for (int i1 = 0; i1 < redBoxX.size(); i1++) {
                if (redBoxX.get(i1) == snakeHeadX && redBoxY.get(i1) == snakeHeadY) {
                    check11 = true;
                }
            }
            for (int i1 = 0; i1 < snakeX.size(); i1++) {
                if ((snakeX.get(i1) == snakeHeadX) && (snakeY.get(i1) == snakeHeadY)) {
                    if (!((snakeX.get(snakeX.size() - 1) == snakeHeadX) && (snakeY.get(snakeY.size() - 1) == snakeHeadY))) {
                        check21 = true;
                    }
                }
            }
            done = check11 || check21;
            snakeX.add(0, snakeHeadX);
            snakeY.add(0, snakeHeadY);
            if (((snakeX.get(0) == fx) && (snakeY.get(0) == fy))) {
                fx = -1;
                fy = -1;
                setTitle("Program" + " : " + snakeX.size());
            } else {
                snakeX.remove(snakeX.size() - 1);
                snakeY.remove(snakeY.size() - 1);
            }
            if (fx == -1) {
                int x, y;
                boolean check1 = false;
                boolean check2 = false;
                do {
                    x = r.nextInt(30);
                    y = r.nextInt(20);
                    for (int i = 0; i < redBoxX.size(); i++) {
                        if (redBoxX.get(i) == x && redBoxY.get(i) == y) {
                            check1 = true;
                        }
                    }
                    for (int i = 0; i < snakeX.size(); i++) {
                        if ((snakeX.get(i) == x) && (snakeY.get(i) == y)) {
                            if (!((snakeX.get(snakeX.size() - 1) == x) && (snakeY.get(snakeY.size() - 1) == y))) {
                                check2 = true;
                            }
                        }
                    }
                } while (check2 || check1);
                fx = x;
                fy = y;
                int x1, y1;
                boolean check3 = false;
                boolean check4 = false;
                do {
                    x1 = r.nextInt(30);
                    y1 = r.nextInt(20);
                    for (int i = 0; i < redBoxX.size(); i++) {
                        if (redBoxX.get(i) == x1 && redBoxY.get(i) == y1) {
                            check3 = true;
                        }
                    }
                    for (int i = 0; i < snakeX.size(); i++) {
                        if ((snakeX.get(i) == x1) && (snakeY.get(i) == y1)) {
                            if (!((snakeX.get(snakeX.size() - 1) == x1) && (snakeY.get(snakeY.size() - 1) == y1))) {
                                check4 = true;
                            }
                        }
                    }
                } while (check3 || check4 || fx == x1 && fy == y1);

                redBoxX.add(x1);
                redBoxY.add(y1);
            }
            c.repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public class Canvas extends JPanel {

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i < snakeX.size(); i++) {
                g.setColor(Color.gray);
                g.fill3DRect(snakeX.get(i) * 25 + 1, snakeY.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            g.setColor(Color.green);
            g.fill3DRect(fx * 25 + 1, fy * 25 + 1, 25 - 2, 25 - 2, true);
            for (int i = 0; i < redBoxX.size(); i++) {
                g.setColor(Color.red);
                g.fill3DRect(redBoxX.get(i) * 25 + 1, redBoxY.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            if (done) {
                g.setColor(Color.red);
                g.setFont(new Font("Arial", Font.BOLD, 60));
                FontMetrics fm = g.getFontMetrics();
                g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
            }
        }
    }

}