package ictgradschool.industry.lab18.ex01;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Write tests
 */
public class TestExcelNew {
    ExcelNew excelNew;
    ArrayList<String> firstNameList;
    ArrayList<String> surnameList;
    String list;
    String[] output;

    @Before
    public void before(){
        excelNew = new ExcelNew();
        firstNameList = new ArrayList<>();
        surnameList = new ArrayList<>();
        excelNew.readFile(firstNameList, surnameList);
        list = excelNew.generateResults(firstNameList, surnameList);
        output = list.split("\\r?\\n");
    }

    @Test
    public void testIDNumber(){
        assertEquals(550, output.length);
    }
//
//    @Test
//    public void testLabResults(){
//
//    }
//    @Test
//    public void testTestResults(){
//
//    }
    @Test
    public void testExamResults(){
        String student = excelNew.generateExamResults("0001",10);
        student = student.substring(4);

        assertTrue((Integer.parseInt(student) >= 40 && (Integer.parseInt(student) <= 49)));
    }
}